Here we present our solution to the CINC 2021 Challenge. 
The last code update removed the MHA layer for the CINC 2021 follow-up. This modification slightly improved the model performance compared to the official round of the challenge.
Code is provided as-is, and no further changes will be included.

Please cite our paper from CINC 2021 and Physiological Measurements 2022 Focus issue (in submission):

P. Nejedly et al, "Classification of ECG using Ensemble of Residual CNNs with or without Attention Mechanism", Physiol Meas. 2022 (in submission) 

P. Nejedly et al., "Classification of ECG Using Ensemble of Residual CNNs with Attention Mechanism," 2021 Computing in Cardiology (CinC), 2021, pp. 1-4, doi: 10.23919/CinC53138.2021.9662723.


This work is based on our submission to CINC 2020:
P. Nejedly, A. Ivora, I. Viscor, J. Halamek, P. Jurak and F. Plesinger, "Utilization of Residual CNN-GRU With Attention Mechanism for Classification of 12-lead ECG," 2020 Computing in Cardiology, 2020, pp. 1-4, doi: 10.22489/CinC.2020.032.
